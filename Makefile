CFLAGS += -O2 -Wall

%.o:	%.c
	$(CC) -c $(CFLAGS) -o $@ $^

%.lo:	%.c
	$(CC) -c -fPIC $(CFLAGS) -o $@ $^

all:	greylstd sqlite_inet.so

install:	all
	install -d $(DESTDIR)/usr/sbin \
		$(DESTDIR)/usr/lib/greylstd \
		$(DESTDIR)/usr/share/doc/greylstd \
		$(DESTDIR)/usr/share/man/man8 \

	install greylstd $(DESTDIR)/usr/sbin/
	install sqlite_inet.so $(DESTDIR)/usr/lib/greylstd/
	install -m 0644 db.sqlite $(DESTDIR)/usr/share/doc/greylstd/
	install -m 0644 UPGRADE $(DESTDIR)/usr/share/doc/greylstd/
	install -m 0644 greylstd.8 $(DESTDIR)/usr/share/man/man8/
	gzip -f $(DESTDIR)/usr/share/man/man8/greylstd.8

clean:
	rm -f *.o *.lo

veryclean:  clean
	rm -f greylstd sqlite_inet.so


sqlite_inet.so:	sqlite_inet.lo
	$(CC) --shared $(CFLAGS) -o $@ $^
	strip --strip-unneeded $@

greylstd:	greylstd.o
	$(CC) $(CFLAGS) -Wl,-rpath -Wl,/usr/lib/greylstd \
		-o $@ $^ -lsqlite3
	strip $@
