/*	-*- C -*-
 * Copyright (C) 2008, Christof Meerwald
 * http://cmeerw.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdint.h>
#include <string.h>

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <netinet/in.h>

#include <sqlite3ext.h>
SQLITE_EXTENSION_INIT1


static void sqlite_inet_pton(sqlite3_context *context,
                             int argc, sqlite3_value **argv)
{
    struct addrinfo hints;
    struct addrinfo *res = NULL;
    const char *name;
    int success = 0;

    if (sqlite3_value_type(argv[0]) == SQLITE_NULL)
    {
        sqlite3_result_null(context);
        return;
    }

    memset(&hints, 0, sizeof(hints));
    hints.ai_flags = AI_NUMERICHOST;

    name = (const char *) sqlite3_value_text(argv[0]);
    if (!getaddrinfo(name, NULL, &hints, &res))
    {
        const struct addrinfo *addr = res;
        while (!success && (addr != NULL))
        {
            if (res->ai_addr->sa_family == AF_INET)
            {
                uint8_t data[5];

                data[0] = '\x04';
                memcpy(data + 1,
                       &((const struct sockaddr_in *) res->ai_addr)->sin_addr.s_addr,
                       4);

                sqlite3_result_blob(context, data, sizeof(data),
                                    SQLITE_TRANSIENT);
                success = 1;
            }
            else if (res->ai_addr->sa_family == AF_INET6)
            {
                uint8_t data[17];

                data[0] = '\x06';
                memcpy(data + 1,
                       &((const struct sockaddr_in6 *) res->ai_addr)->sin6_addr.s6_addr,
                       16);

                sqlite3_result_blob(context, data, sizeof(data),
                                    SQLITE_TRANSIENT);
                success = 1;
            }
        }

        freeaddrinfo(res);
    }

    if (!success)
    {
        sqlite3_result_error(context, "invalid IP address", -1);
    }
}

static void sqlite_inet_ntop(sqlite3_context *context,
                             int argc, sqlite3_value **argv)
{
    const uint8_t *addr;
    int addrlen;
    int success = 0;

    if (sqlite3_value_type(argv[0]) == SQLITE_NULL)
    {
        sqlite3_result_null(context);
        return;
    }

    addr = (const uint8_t *) sqlite3_value_blob(argv[0]);
    addrlen = sqlite3_value_bytes(argv[0]);

    if ((addrlen == 5) && (addr[0] == '\x04'))
    {
        char name[INET_ADDRSTRLEN];
        if (inet_ntop(AF_INET, addr + 1, name, sizeof(name)))
        {
            sqlite3_result_text(context, name, strlen(name),
                                SQLITE_TRANSIENT);
            success = 1;
        }
    }
    else if ((addrlen == 17) && (addr[0] == '\x06'))
    {
        char name[INET6_ADDRSTRLEN];
        if (inet_ntop(AF_INET6, addr + 1, name, sizeof(name)))
        {
            sqlite3_result_text(context, name, strlen(name),
                                SQLITE_TRANSIENT);
            success = 1;
        }
    }

    if (!success)
    {
        sqlite3_result_error(context, "invalid IP address", -1);
    }
}

static int bitmask_contained_in(const uint8_t *data,
                                const uint8_t *pattern,
                                int len, int prefixlen)
{
    const int prefixbytes = prefixlen / 8;
    int i;

    for (i = 0; i < prefixbytes; i++)
    {
        if (pattern[i] != data[i])
        {
            return 0;
        }
    }

    if (i < len)
    {
        const int mask = -1 << (8 - (prefixlen % 8));
        if (((data[i] ^ pattern[i]) & mask) ||
            (pattern[i] & ~mask))
        {
            return 0;
        }

        i++;
    }

    for (; i < len; i++)
    {
        if (pattern[i] != 0)
        {
            return 0;
        }
    }

    return 1;
}

static void sqlite_inet_contained_in(sqlite3_context *context,
                                     int argc, sqlite3_value **argv)
{
    const uint8_t *addr = (const uint8_t *) sqlite3_value_blob(argv[0]);
    const uint8_t *network = (const uint8_t *) sqlite3_value_blob(argv[1]);
    const int addrlen = sqlite3_value_bytes(argv[0]);
    const int networklen = sqlite3_value_bytes(argv[1]);
    const int prefixlen = sqlite3_value_int(argv[2]);

    if ((addrlen == 5) && (networklen == 5) &&
        (addr[0] == '\x04') && (network[0] == '\x04'))
    {
        sqlite3_result_int(context,
                           bitmask_contained_in(addr + 1, network + 1, 4,
                                                prefixlen));
    }
    else if ((addrlen == 17) && (networklen == 17) &&
             (addr[0] == '\x06') && (network[0] == '\x06'))
    {
        sqlite3_result_int(context,
                           bitmask_contained_in(addr + 1, network + 1, 16,
                                                prefixlen));
    }
    else
    {
        sqlite3_result_error(context, "invalid IP address", -1);
    }
}


int sqlite3_extension_init(sqlite3 *db, char **pzErrMsg,
                           const sqlite3_api_routines *pApi)
{
    SQLITE_EXTENSION_INIT2(pApi)

    sqlite3_create_function(db, "inet_pton", 1, SQLITE_UTF8, 0,
                            sqlite_inet_pton, 0, 0);
    sqlite3_create_function(db, "inet_ntop", 1, SQLITE_UTF8, 0,
                            sqlite_inet_ntop, 0, 0);
    sqlite3_create_function(db, "inet_contained_in", 3, SQLITE_UTF8, 0,
                            sqlite_inet_contained_in, 0, 0);

    return SQLITE_OK;
}
