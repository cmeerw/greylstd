-- -*- SQL -*-
-- Copyright (C) 2006-2018, Christof Meerwald
-- http://cmeerw.org

-- This program is free software; you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation; version 2 dated June, 1991.
--
-- This program is distributed in the hope that it will be useful, but
-- WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
-- General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program; if not, write to the Free Software
-- Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
-- USA

CREATE TABLE greyconf (
    id INTEGER PRIMARY KEY,
    recipient TEXT NOT NULL DEFAULT '',
    minwait INTEGER DEFAULT NULL,
    maxwait INTEGER DEFAULT NULL,
    maxvalid INTEGER DEFAULT NULL,
    UNIQUE (recipient)
);

CREATE INDEX greyconf_recipient
    ON greyconf (recipient);

CREATE TABLE greylist (
    id INTEGER PRIMARY KEY,
    ctime INTEGER NOT NULL,
    atime INTEGER DEFAULT NULL,
    remoteip BLOB NOT NULL,
    sender TEXT NOT NULL,
    recipient TEXT NOT NULL,
    UNIQUE (remoteip, sender, recipient)
);

CREATE INDEX greylist_index
    ON greylist (remoteip, recipient, atime);

CREATE TABLE whitelist (
    id INTEGER PRIMARY KEY,
    recipient TEXT NOT NULL DEFAULT '',
    sender TEXT NOT NULL DEFAULT '',
    remoteip BLOB,
    remoteprefixlen INTEGER
);

CREATE INDEX whitelist_index
    ON whitelist (remoteip, remoteprefixlen, recipient, sender);
