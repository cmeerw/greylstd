/*	-*- C -*-
 * Copyright (C) 2006-2018, Christof Meerwald
 * http://cmeerw.org
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <libgen.h>

#include <fcntl.h>
#include <syslog.h>
#include <time.h>
#include <unistd.h>

#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>

#include <sqlite3.h>


#define RETRY_EINTR_RC(rc, expr) \
    { \
        do \
        { \
            rc = (expr); \
        } while ((rc < 0) && (errno == EINTR) && !quit); \
    }

#define RETRY_EINTR(expr) \
    while (((expr) < 0) && (errno == EINTR) && !quit) \
    { }


static const int READ_TIMEOUT = 10000;
static const time_t CLEANUP_INTERVAL = 6 * 60 * 60;

static const int MAX_CTIME_VALID = 24 * 60 * 60;
static const int MAX_ATIME_VALID = 30 * 24 * 60 * 60;

static const int DEFAULT_CTIME_WAIT = 2 * 60;
static const int DEFAULT_CTIME_VALID = 12 * 60 * 60;
static const int DEFAULT_ATIME_VALID = 14 * 24 * 60 * 60;

static sqlite3 *db = NULL;
static int verbose = 0;
static volatile sig_atomic_t quit = 0;
static int pidfd = -1;


enum Sql_Statements
{
    SELECT_GREYLIST_EXACT,
    SELECT_GREYLIST_IP,
    INSERT_GREYLIST,
    UPDATE_GREYLIST,
    SELECT_INET_ADDRESS,
    SELECT_WHITELIST,
    SELECT_GREYCONFIG,
    DELETE_GREYLIST
};

static const char * const sql_queries[] = {
    "SELECT id, ctime, atime FROM greylist"
    "    WHERE remoteip=? AND sender=? AND recipient=?",

    "SELECT COUNT(*) FROM greylist"
    "    WHERE remoteip=? AND recipient=?"
    "    AND atime > ?",

    "INSERT INTO greylist (ctime, atime, remoteip, sender, recipient)"
    "    VALUES (?, ?, ?, ?, ?)",

    "UPDATE greylist SET ctime=?, atime=? WHERE id=?",

    "SELECT inet_pton(?)",

    "SELECT 1 WHERE EXISTS"
    "    (SELECT id FROM whitelist WHERE remoteip IS NULL AND"
    "        remoteprefixlen IS NULL AND"
    "        recipient IN ('', :rdomain, :recipient) AND"
    "        sender IN ('', :sdomain, :sender)) OR"
    "    EXISTS"
    "    (SELECT id FROM whitelist WHERE"
    "        remoteip <= :remoteip AND remoteip > :addrtype AND"
    "        inet_contained_in(:remoteip, remoteip, remoteprefixlen) AND"
    "        recipient IN ('', :rdomain, :recipient) AND"
    "        sender IN ('', :sdomain, :sender)"
    "    ORDER BY remoteip DESC, remoteprefixlen DESC, recipient DESC,"
    "        sender DESC)",

    "SELECT minwait, maxwait, maxvalid FROM greyconf"
    "    WHERE recipient IN ('', ?, ?) ORDER BY length(recipient)",

    "DELETE FROM greylist"
    "    WHERE (atime < :atime) OR (ctime < :ctime AND atime IS NULL)"
};

static sqlite3_stmt *stmt[sizeof(sql_queries) / sizeof(*sql_queries)] = { NULL };


/**
 * Set quit indicator.
 */
static void signal_quit(int signo)
{
    quit = 1;
}


void update_greylist(int id, time_t ctime, time_t atime)
{
    sqlite3_bind_int(stmt[UPDATE_GREYLIST], 1, ctime);
    if (atime)
    {
        sqlite3_bind_int64(stmt[UPDATE_GREYLIST], 2, atime);
    }
    else
    {
        sqlite3_bind_null(stmt[UPDATE_GREYLIST], 2);
    }
    sqlite3_bind_int(stmt[UPDATE_GREYLIST], 3, id);

    if (SQLITE_DONE != sqlite3_step(stmt[UPDATE_GREYLIST]))
    {
        syslog(LOG_WARNING, "Error updating greylist ctime: %s",
               sqlite3_errmsg(db));
    }

    sqlite3_reset(stmt[UPDATE_GREYLIST]);
}

/**
 * Perform a greylist check.
 */
void check_cmd(int sock, const char *remoteip, const char *sender,
               const char *recipient)
{
    int rc, off, l;
    int result = 0;
    int whitelisted = 0;
    int remoteaddrlen = 0;
    const unsigned char *remoteaddr = NULL;
    const char *rdomain = strchr(recipient, '@');
    const char *sdomain = strchr(sender, '@');
    const char *response = NULL;
    const time_t now = time(NULL);

    if ((rdomain == NULL) || (sdomain == NULL))
    {
        return;
    }

    sqlite3_bind_text(stmt[SELECT_INET_ADDRESS], 1, remoteip, strlen(remoteip),
                      SQLITE_STATIC);
    if ((rc = sqlite3_step(stmt[SELECT_INET_ADDRESS])) == SQLITE_ROW)
    {
        remoteaddr = sqlite3_column_blob(stmt[SELECT_INET_ADDRESS], 0);
        remoteaddrlen = sqlite3_column_bytes(stmt[SELECT_INET_ADDRESS], 0);
    }
    else
    {
        syslog(LOG_WARNING, "Error getting IP address: %s",
               sqlite3_errmsg(db));
    }

    if (remoteaddrlen >= 1)
    {
        sqlite3_bind_text(stmt[SELECT_WHITELIST], 1, rdomain,
                          strlen(rdomain), SQLITE_STATIC);
        sqlite3_bind_text(stmt[SELECT_WHITELIST], 2, recipient,
                          strlen(recipient), SQLITE_STATIC);

        sqlite3_bind_text(stmt[SELECT_WHITELIST], 3, sdomain,
                          strlen(sdomain), SQLITE_STATIC);
        sqlite3_bind_text(stmt[SELECT_WHITELIST], 4, sender,
                          strlen(sender), SQLITE_STATIC);

        sqlite3_bind_blob(stmt[SELECT_WHITELIST], 5, remoteaddr,
                          remoteaddrlen, SQLITE_STATIC);
        sqlite3_bind_blob(stmt[SELECT_WHITELIST], 6, remoteaddr,
                          1, SQLITE_STATIC);

        if ((rc = sqlite3_step(stmt[SELECT_WHITELIST])) == SQLITE_ROW)
        {
            whitelisted = sqlite3_column_int(stmt[SELECT_WHITELIST], 0);
            result = whitelisted;
        }
        else if (rc != SQLITE_DONE)
        {
            syslog(LOG_WARNING, "Error querying whitelist: %s",
                   sqlite3_errmsg(db));
        }

        sqlite3_reset(stmt[SELECT_WHITELIST]);
    }

    if (!whitelisted)
    {
        int minwait = DEFAULT_CTIME_WAIT;
        int maxwait = DEFAULT_CTIME_VALID;
        int maxvalid = DEFAULT_ATIME_VALID;

        sqlite3_bind_text(stmt[SELECT_GREYCONFIG], 1,
                          recipient, strlen(recipient), SQLITE_STATIC);
        sqlite3_bind_text(stmt[SELECT_GREYCONFIG], 2,
                          rdomain, strlen(rdomain), SQLITE_STATIC);
        while ((rc = sqlite3_step(stmt[SELECT_GREYCONFIG])) == SQLITE_ROW)
        {
            if (sqlite3_column_type(stmt[SELECT_GREYCONFIG], 0) != SQLITE_NULL)
            {
                minwait = sqlite3_column_int(stmt[SELECT_GREYCONFIG], 0);
            }

            if (sqlite3_column_type(stmt[SELECT_GREYCONFIG], 1) != SQLITE_NULL)
            {
                maxwait = sqlite3_column_int(stmt[SELECT_GREYCONFIG], 1);
            }

            if (sqlite3_column_type(stmt[SELECT_GREYCONFIG], 2) != SQLITE_NULL)
            {
                maxvalid = sqlite3_column_int(stmt[SELECT_GREYCONFIG], 2);
            }
        }

        sqlite3_reset(stmt[SELECT_GREYCONFIG]);

        sqlite3_bind_blob(stmt[SELECT_GREYLIST_EXACT], 1, remoteaddr,
                          remoteaddrlen, SQLITE_STATIC);
        sqlite3_bind_text(stmt[SELECT_GREYLIST_EXACT], 2,
                          sender, strlen(sender), SQLITE_STATIC);
        sqlite3_bind_text(stmt[SELECT_GREYLIST_EXACT], 3,
                          recipient, strlen(recipient), SQLITE_STATIC);

        int row_id = 0;
        time_t update_ctime = 0;
        time_t update_atime = 0;

        if ((rc = sqlite3_step(stmt[SELECT_GREYLIST_EXACT])) == SQLITE_ROW)
        {
            row_id = sqlite3_column_int(stmt[SELECT_GREYLIST_EXACT], 0);
            const time_t ctime = sqlite3_column_int64(stmt[SELECT_GREYLIST_EXACT], 1);
            const time_t atime = sqlite3_column_int64(stmt[SELECT_GREYLIST_EXACT], 2);

            if (sqlite3_step(stmt[SELECT_GREYLIST_EXACT]) != SQLITE_DONE)
            {
                syslog(LOG_WARNING, "Error querying greylist database: %s",
                       sqlite3_errmsg(db));
            }

            if (atime == 0)
            {
                if (now - ctime < minwait)
                {
                    // nothing to do
                }
                else if (now - ctime > maxwait)
                {
                    // have to update ctime
                    update_ctime = now;
                }
                else
                {
                    result = 1;
                    update_ctime = ctime;
                    update_atime = now;
                }
            }
            else if (now - atime < maxvalid)
            {
                result = 1;

                // have to update atime
                update_ctime = ctime;
                update_atime = atime;
            }
            else
            {
                // have to update ctime, reset atime
                update_ctime = now;
            }
        }
        else if (rc != SQLITE_DONE)
        {
            syslog(LOG_WARNING, "Error querying greylist database: %s",
                   sqlite3_errmsg(db));

            // be conservative and accept mail when having problems with
            // our database
            result = 1;
        }
        sqlite3_reset(stmt[SELECT_GREYLIST_EXACT]);

        if (!result)
        {
            sqlite3_bind_blob(stmt[SELECT_GREYLIST_IP], 1, remoteaddr,
                              remoteaddrlen, SQLITE_STATIC);
            sqlite3_bind_text(stmt[SELECT_GREYLIST_IP], 2,
                              recipient, strlen(recipient), SQLITE_STATIC);
            sqlite3_bind_int(stmt[SELECT_GREYLIST_IP], 3, now - maxvalid);

            if ((rc = sqlite3_step(stmt[SELECT_GREYLIST_IP])) == SQLITE_ROW)
            {
                result = sqlite3_column_int(stmt[SELECT_GREYLIST_IP], 0) > 0;
            }

            sqlite3_reset(stmt[SELECT_GREYLIST_IP]);

            if (result)
            {
                update_atime = now;
            }
        }

        if (row_id)
        {
            if (update_ctime || update_atime)
            {
                update_greylist(row_id,
                                update_ctime ? update_ctime : update_atime,
                                update_atime);
            }
        }
        else if (!result || update_atime)
        {
            // not found in database, create new record
            sqlite3_bind_int(stmt[INSERT_GREYLIST], 1, now);
            if (result)
            {
                sqlite3_bind_int64(stmt[INSERT_GREYLIST], 2, now);
            }
            else
            {
                sqlite3_bind_null(stmt[INSERT_GREYLIST], 2);
            }

            sqlite3_bind_blob(stmt[INSERT_GREYLIST], 3,
                              remoteaddr, remoteaddrlen,
                              SQLITE_STATIC);
            sqlite3_bind_text(stmt[INSERT_GREYLIST], 4,
                              sender, strlen(sender), SQLITE_STATIC);
            sqlite3_bind_text(stmt[INSERT_GREYLIST], 5,
                              recipient, strlen(recipient), SQLITE_STATIC);

            if (SQLITE_DONE != sqlite3_step(stmt[INSERT_GREYLIST]))
            {
                syslog(LOG_WARNING, "Error inserting greylist record: %s",
                       sqlite3_errmsg(db));
            }

            sqlite3_reset(stmt[INSERT_GREYLIST]);
        }
    }

    sqlite3_reset(stmt[SELECT_INET_ADDRESS]);

    response = result ? "accept\n" : "defer\n";
    l = strlen(response);
    off = 0;

    while (off < l)
    {
        rc = write(sock, response + off, l - off);
        if ((rc < 0) && (errno != EINTR))
        {
            syslog(LOG_WARNING, "write: %m");
            break;
        }
        else if (rc > 0)
        {
            off += rc;
        }
    }

    return;
}

/**
 * Parse the command received on the socket and process it.
 * Supported commands:
 * - check <remoteip> <sender> <recipient>
 * - quit
 *
 * @return 1 to close the connection
 */
int process(int sock, char *p)
{
    if (!strncmp(p, "check ", 6))
    {
        const char *remoteip;
        const char *sender;
        const char *recipient;

        p += 6;
        remoteip = p;

        p = strchr(p, ' ');
        if (p == NULL)
        {
            return 1;
        }
        *p++ = '\0';
        sender = p;

        p = strchr(p, ' ');
        if (p == NULL)
        {
            return 1;
        }
        *p++ = '\0';
        recipient = p;

        p = strchr(p, ' ');
        if (p)
        {
            return 1;
        }

        check_cmd(sock, remoteip, sender, recipient);
        return 1;
    }
    else if (!strcmp(p, "quit"))
    {
        syslog(LOG_INFO, "shutting down");
        quit = 1;
        return 1;
    }

    syslog(LOG_WARNING, "unknown command received, ignoring");
    return 1;
}

/**
 * Perform periodic database maintenance by deleting expired records.
 */
void perform_db_cleanup(void)
{
    const time_t now = time(NULL);

    if (verbose)
    {
        syslog(LOG_DEBUG, "performing database cleanup");
    }

    sqlite3_bind_int64(stmt[DELETE_GREYLIST], 1, now - MAX_ATIME_VALID);
    sqlite3_bind_int64(stmt[DELETE_GREYLIST], 2, now - MAX_CTIME_VALID);
    if (SQLITE_DONE != sqlite3_step(stmt[DELETE_GREYLIST]))
    {
        syslog(LOG_WARNING, "Error cleaning up database: %s",
               sqlite3_errmsg(db));
    }
    sqlite3_reset(stmt[DELETE_GREYLIST]);
}

/**
 * Daemonize the process.
 *
 * @return pipe-fd that should be closed once initialization is complete
 */
int daemonize(void)
{
    int pipefds[2];
    int fd;
    int rc;
    char buf;

    if (pipe(pipefds) < 0)
    {
        syslog(LOG_ALERT, "pipe: %m");
        return -1;
    }

    switch (fork())
    {
    case 0:
        break;

    case -1:
        // error
        syslog(LOG_ALERT, "fork: %m");
        RETRY_EINTR(close(pipefds[0]));
        RETRY_EINTR(close(pipefds[1]));
        return -1;

    default:
        // parent
        RETRY_EINTR(close(pipefds[1]));

        RETRY_EINTR_RC(rc, read(pipefds[0], &buf, sizeof(buf)));
        if ((rc == sizeof(buf)) && (buf == '\0'))
        {
            RETRY_EINTR(close(pipefds[0]));
            closelog();
            _exit(0);
        }
        else if (rc < 0)
        {
            syslog(LOG_ALERT, "read: %m");
        }
        else
        {
            syslog(LOG_ALERT, "daemon initialisation failed");
        }

        RETRY_EINTR(close(pipefds[0]));
        closelog();
        exit(1);
    }

    RETRY_EINTR(close(pipefds[0]));

    if (setsid() < 0)
    {
        syslog(LOG_ERR, "setsid: %m");
    }

    switch (fork())
    {
    case 0:
        break;

    case -1:
        // error
        syslog(LOG_ALERT, "fork: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;

    default:
        // parent
        _exit(0);
    }


    RETRY_EINTR_RC(fd, open("/dev/null", O_RDWR));
    if (fd < 0)
    {
        syslog(LOG_ALERT, "open /dev/null: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR_RC(rc, dup2(fd, STDIN_FILENO));
    if (rc < 0)
    {
        syslog(LOG_ALERT, "dup2 STDIN: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR_RC(rc, dup2(fd, STDOUT_FILENO));
    if (rc < 0)
    {
        syslog(LOG_ALERT, "dup2 STDOUT: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR_RC(rc, dup2(fd, STDERR_FILENO));
    if (rc < 0)
    {
        syslog(LOG_ALERT, "dup2 STDERR: %m");
        RETRY_EINTR(close(pipefds[1]));
        return -1;
    }

    RETRY_EINTR(close(fd));

    return pipefds[1];
}

/**
 * Daemon initialization including pid-file handling.
 */
int daemon_init(const char *pidname)
{
    struct flock fl;
    char buf[32];
    int rc, l;

    if (chdir("/") < 0)
    {
        syslog(LOG_ERR, "chdir: %m");
    }

    umask(0);

    RETRY_EINTR_RC(pidfd, open(pidname, O_RDWR | O_CREAT, 0644));
    if (pidfd < 0)
    {
        syslog(LOG_ALERT, "open pid file: %m");
        return -1;
    }

    fl.l_type = F_WRLCK;
    fl.l_whence = SEEK_SET;
    fl.l_start = 0;
    fl.l_len = 0;

    if (fcntl(pidfd, F_SETLK, &fl) < 0)
    {
        if ((errno == EACCES) || (errno == EAGAIN))
        {
            syslog(LOG_ERR, "pid file already locked");
        }
        else
        {
            syslog(LOG_ALERT, "fcntl SETLK: %m");
        }

        RETRY_EINTR(close(pidfd));
        pidfd = -1;
        return -1;
    }

    RETRY_EINTR_RC(rc, ftruncate(pidfd, 0));
    if (rc < 0)
    {
        RETRY_EINTR(close(pidfd));
        pidfd = -1;
        return -1;
    }

    l = snprintf(buf, sizeof(buf), "%d\n", getpid());
    RETRY_EINTR_RC(rc, write(pidfd, buf, l));
    if (rc != l)
    {
        syslog(LOG_ALERT, "writing pid file: %m");
        RETRY_EINTR(close(pidfd));
        pidfd = -1;
        return -1;
    }

    return 0;
}


int main(int argc, char *argv[])
{
    struct sockaddr_un addr;
    socklen_t addr_len = sizeof(addr);
    struct sigaction act;
    int lsock;
    int daemonpipe = -1;
    int c;
    unsigned int i;
    time_t next_cleanup;
    const char *tail;
    char *err_msg = NULL;

    const char *dbname = "/var/lib/greylstd/greylstd.db";
    int foreground = 0;
    const char *pidname = "/run/greylstd/greylstd.pid";
    const char *sockname = "/run/greylstd/greylstd.sock";
    const char *progname = basename(argv[0]);

    extern char *optarg;
    extern int optind, optopt;


    while ((c = getopt(argc, argv, "d:fhp:s:v")) != -1)
    {
        switch (c)
        {
        case 'd':
            dbname = optarg;
            break;

        case 'f':
            foreground = 1;
            break;

        case 'h':
            fprintf(stderr, "%s [-f] [-v] [-d dbname] [-p pidfile] [-s sockname]\n", progname);
            quit = 1;
            break;

        case 'p':
            pidname = optarg;
            break;

        case 's':
            sockname = optarg;
            break;

        case 'v':
            verbose = 1;
            break;

        case '?':
            fprintf(stderr, "Unrecognised option -%c\n", optopt);
            quit = 1;
            break;
        }
    }

    if (quit)
    {
        return 1;
    }

    openlog(progname, LOG_NDELAY, LOG_MAIL);

    act.sa_handler = &signal_quit;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGINT, &act, NULL) < 0)
    {
        syslog(LOG_ERR, "sigaction: %m");
    }

    act.sa_handler = &signal_quit;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGTERM, &act, NULL) < 0)
    {
        syslog(LOG_ERR, "sigaction: %m");
    }

    act.sa_handler = SIG_IGN;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGPIPE, &act, NULL) < 0)
    {
        syslog(LOG_ERR, "sigaction: %m");
    }

    act.sa_handler = SIG_IGN;
    sigemptyset(&act.sa_mask);
    act.sa_flags = 0;
    if (sigaction(SIGHUP, &act, NULL) < 0)
    {
        syslog(LOG_ERR, "sigaction: %m");
    }

    if (!foreground)
    {
        if ((daemonpipe = daemonize()) < 0)
        {
            return 1;
        }
    }

    if (daemon_init(pidname))
    {
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    if (SQLITE_OK != sqlite3_open(dbname, &db))
    {
        syslog(LOG_ALERT, "Error opening SQLite database (%s)",
               sqlite3_errmsg(db));
        sqlite3_close(db);
        closelog();
        RETRY_EINTR(close(pidfd));
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    sqlite3_enable_load_extension(db, 1);
    sqlite3_busy_timeout(db, 10000);

    if (SQLITE_OK != sqlite3_load_extension(db,
                                            "/usr/lib/greylstd/sqlite_inet.so",
                                            NULL, &err_msg))
    {
        syslog(LOG_ALERT, "Error loading sqlite_inet extension (%s)",
               err_msg);
        sqlite3_free(err_msg);
        err_msg = NULL;
    }

    for (i = 0; i < sizeof(sql_queries) / sizeof(*sql_queries); i++)
    {
        if (SQLITE_OK != sqlite3_prepare(db, sql_queries[i],
                                         strlen(sql_queries[i]),
                                         &stmt[i], &tail)) {
            syslog(LOG_ALERT, "Error preparing SQL query (%s)",
                   sqlite3_errmsg(db));
            sqlite3_close(db);
            closelog();
            RETRY_EINTR(close(pidfd));
            if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
            return 1;
        }
    }


    perform_db_cleanup();
    next_cleanup = time(NULL) + CLEANUP_INTERVAL;

    lsock = socket(PF_UNIX, SOCK_STREAM, 0);
    if (lsock < 0)
    {
        syslog(LOG_ALERT, "socket: %m");
        sqlite3_close(db);
        closelog();
        RETRY_EINTR(close(pidfd));
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    // unlink the socket address - just in case
    unlink(sockname);

    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, sockname);
    if (bind(lsock, (struct sockaddr *) &addr, SUN_LEN(&addr)) < 0)
    {
        syslog(LOG_ALERT, "bind: %m");
        sqlite3_close(db);
        closelog();
        RETRY_EINTR(close(pidfd));
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    if (listen(lsock, 10) < 0)
    {
        syslog(LOG_ALERT, "listen: %m");
        sqlite3_close(db);
        closelog();
        RETRY_EINTR(close(pidfd));
        if (daemonpipe >= 0) RETRY_EINTR(close(daemonpipe));
        return 1;
    }

    if (daemonpipe >= 0)
    {
        int rc;

        RETRY_EINTR_RC(rc, write(daemonpipe, "\0", 1));
        if (rc < 0)
        {
            syslog(LOG_ALERT, "write daemon pipe: %m");
            sqlite3_close(db);
            closelog();
            RETRY_EINTR(close(pidfd));
            RETRY_EINTR(close(daemonpipe));
            return 1;
        }

        RETRY_EINTR(close(daemonpipe));
        daemonpipe = -1;
    }

    while (!quit)
    {
        struct pollfd pfd = { lsock, POLLIN, 0 };
        int sock;
        char buf[4096];
        int len = 0;
        int rc, flags;
        const time_t now = time(NULL);
        int timeout = next_cleanup - now;

        if (timeout < 3)
        {
            perform_db_cleanup();
            next_cleanup = now + CLEANUP_INTERVAL;
            continue;
        }

        RETRY_EINTR_RC(rc, poll(&pfd, 1, timeout * 1000));
        if (rc < 0)
        {
            if (!quit)
            {
                syslog(LOG_ERR, "poll: %m");
            }
            break;
        }
        else if (rc == 0)
        {
            // poll timed out
            continue;
        }

        RETRY_EINTR_RC(sock, accept(lsock, (struct sockaddr *) &addr,
                                    &addr_len));
        if (sock < 0)
        {
            if (!quit)
            {
                syslog(LOG_ERR, "accept: %m");
                continue;
            }
            else
            {
                break;
            }
        }

        if (verbose)
        {
            syslog(LOG_DEBUG, "new connection");
        }

        // set non-blocking mode
        flags = fcntl(sock, F_GETFL, 0);
        if (flags < 0)
        {
            syslog(LOG_ERR, "fcntl: %m");
            RETRY_EINTR(close(sock));
            continue;
        }
        if (fcntl(sock, F_SETFL, flags | O_NONBLOCK) < 0)
        {
            syslog(LOG_ERR, "fcntl: %m");
            RETRY_EINTR(close(sock));
            continue;
        }

        for ( ; ; )
        {
            int rc;
            char *p;
            struct pollfd pfd = { sock, POLLIN, 0 };

            RETRY_EINTR_RC(rc, poll(&pfd, 1, READ_TIMEOUT));
            if (rc < 0)
            {
                if (!quit)
                {
                    syslog(LOG_ERR, "poll: %m");
                }
                break;
            }
            else if (rc == 0)
            {
                // timed out
                syslog(LOG_INFO, "timeout, closing connection");
                break;
            }

            RETRY_EINTR_RC(rc, read(sock, buf + len, sizeof(buf) - len - 1));
            if (rc < 0)
            {
                if (!quit)
                {
                    syslog(LOG_ERR, "read: %m");
                }
                break;
            }
            else if (rc == 0)
            {
                RETRY_EINTR(close(sock));
                break;
            }

            len += rc;
            if (len == sizeof(buf) - 1)
            {
                // buffer overrun
                syslog(LOG_ERR, "buffer overrun, closing connection");
                break;
            }

            buf[len] = '\0';
            p = strchr(buf, '\n');
            if (p == NULL)
            {
                continue;
            }

            *p++ = '\0';
            if (verbose)
            {
                syslog(LOG_DEBUG, "%s", buf);
            }

            // process command
            if (process(sock, buf))
            {
                break;
            }

            len -= p - buf;
            memmove(buf, p, len);
        }

        if (verbose)
        {
            syslog(LOG_DEBUG, "closing connection");
        }
        RETRY_EINTR(close(sock));
    }

    sqlite3_close(db);
    closelog();
    RETRY_EINTR(close(pidfd));

    return 0;
}
